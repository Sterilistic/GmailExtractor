function get_lists() {
    // fetching the list of all labels 

    $.ajax({
        type: "GET",
        url: "/api/list_labels?format=json",
        success: function(labels) {
            var data = labels;
            data.sortOn("name");
            var select_labels = document.getElementById("all_labels");
            for (var i = 0; i < data.length; i++) {
                if((data[i].name).indexOf('/') > -1){
                    var nestLabel = data[i].name.substr(data[i].name.indexOf("/") + 1);
                    var slicedLabel = data[i].name.substr(0, data[i].name.indexOf('/'));
                    var parent_label = document.getElementById(slicedLabel);
                    var parent_link = document.getElementById(slicedLabel+"link");
                    var expand_ico = document.createElement('span');
                    expand_ico.setAttribute('class', 'fa fa-plus-square-o');
                    expand_ico.setAttribute('data-toggle', 'collapse');
                    expand_ico.style.float="right";
                    expand_ico.id = "plssave";
                    expand_ico.setAttribute('data-target', '#'+data[i].id);
                    var listBox = document.createElement('div');
                    listBox.id = data[i].id;
                    listBox.setAttribute('class', 'collapse');
                    var nestList = document.createElement('ul');
                    nestList.setAttribute("class", "nav nav-second-level");
                    var label_list = document.createElement('li');
                    var label_link = document.createElement('a');
                    label_list.id =  data[i].name;
                    var label = nestLabel.capitalizeFirstLetter();
                    label_link.innerHTML = label.capitalizeFirstLetter();
                    label_link.href = "javascript:switch_label(\"" + data[i].id + "\",\"" + data[i].name + "\");";
                    parent_link.appendChild(expand_ico);
                    label_list.appendChild(label_link);
                    nestList.appendChild(label_list);
                    listBox.appendChild(nestList);
                    parent_label.appendChild(listBox);
                }
                else{


                    var label_list = document.createElement('li');
                    label_list.id = data[i].name;
                    var label_link = document.createElement('a');
                    label_link.id = data[i].name+"link";
                    var label = data[i].name;
                    label_link.innerHTML = label.capitalizeFirstLetter();
                    label_link.href = "javascript:switch_label(\"" + data[i].id + "\",\"" + data[i].name + "\");";
                    label_list.appendChild(label_link);
                    select_labels.appendChild(label_list);
                }        
            }

        },
        error: function(data, errorThrown) {
            alert("Error:" + errorThrown + ":" + data);
        }
    });
}

Array.prototype.sortOn = function(key){
    this.sort(function(a, b){
        if(a[key] < b[key]){
            return -1;
        }else if(a[key] > b[key]){
            return 1;
        }
        return 0;
    });
}



String.prototype.capitalizeFirstLetter = function() {
    refined_label = this.replace(/_/g,' ').toLowerCase()
    changed_case  =  refined_label.charAt(0).toUpperCase() + refined_label.slice(1)
    return changed_case;
}


//Getting the user_id
function get_user() {

	//getting the user information

    $.ajax({
        type: "GET",
        url: "/api/get_user?format=json",
        success: function(data) {
            var current_user = document.getElementById('myName');
            var ico = document.createElement('i');
            ico.setAttribute("class", "fa fa-user");
            current_user.innerHTML = data;

        },
    });
}

//Getting the list of requested folders download
function get_completed_download(){
	var table =document.getElementById("requested_downloads");
	//table.innerHTML = "";
	$.ajax({
		type: "GET",
		url: "/bulkdownload/list_downloads/",
		success: function(data){
			var select = ["user_id", "sender", "subject", "body"];
			for(var i= 0;i<data.length;i++){
					var row = document.createElement("tr");
					row.id=data[i].label;
					var label_name = document.createElement("td");
					label_name.innerHTML=data[i].label;
					var request_date = document.createElement("td");
					request_date.innerHTML=data[i].date;
					var options = document.createElement("td");
					for(var j=0;j<select.length;j++){
						var checkbox = document.createElement("input");
						checkbox.type = "checkbox";
						checkbox.name = data[i].label+"option";
						checkbox.value = select[j];
						checkbox.id = data[i].label+select[j];
						var checkbox_label = document.createElement('label');
						checkbox_label.setAttribute("style", "margin-right:10px;")
						checkbox_label.htmlFor = data[i].label+select[j];
						if (select[j]=="user_id"){
							checkbox_label.appendChild(document.createTextNode("To"));
						}
						else{
							checkbox_label.appendChild(document.createTextNode(select[j].capitalizeFirstLetter()));	
						}
						options.appendChild(checkbox);
						options.appendChild(checkbox_label);
					}
					var file_name = document.createElement('td');
						file_name.innerHTML='<input id ="'+data[i].label+'filename" value ="'+data[i].label+'" type="text" class="form-control col-xs-5" style ="margin-right:20px; margin-left:0px;" placeholder="Enter the file name:">'
					if (data[i].flag==0){
					var actions = document.createElement("td");
					actions.innerHTML='<a style="margin:5px;" id = "json" href="javascript:test(\''+data[i].label+'\',\'json\')" class="btn btn-info btn-sm" role="button">Download JSON</a><a style="margin:5px;" href="javascript:test(\''+data[i].label+'\',\'csv\')" class="btn btn-info btn-sm" role="button">Download CSV</a><input style="margin:5px;" id ="demo2" class="btn btn-danger btn-sm" type="button" value="Delete"  onclick="bulk_download_json(\''+data[i].label+'\')" />'
					}
					else{
						var actions = document.createElement("td");
						actions.innerHTML='<a style="margin:5px; background-color:darkgray" href="#" class="btn btn-info btn-sm" role="button">Pending..</a>';
					}
					row.appendChild(label_name);
					row.appendChild(request_date);
					row.appendChild(options);
					row.appendChild(file_name)
					row.appendChild(actions);
					table.appendChild(row);
			}
		},
	});
}



//Showing up the accordion of mails
selected_mails = [];
function previous_current_mails(data) {
	//displaying the current mails
    current_mails = data; 
    var mails_accordion = document.getElementById("mails_accordion");
    var norecordfound = document.getElementById("norecordfound");
    mails_accordion.innerHTML = "";
    norecordfound.style.display = "None";
    
    if (current_mails.length > 0) {
        for (var i = 0; i < current_mails.length; i++) {
            // accordian list
            var accordion_defpan = document.createElement('div');
            accordion_defpan.setAttribute("class", "panel panel-default");
            var headpan = document.createElement('div');
            headpan.setAttribute("class", "panel-heading");
            headpan.setAttribute("role", "tab");
            headpan.setAttribute("id", "headingOne");
            headpan.setAttribute("style",'style="text-overflow: ellipsis;white-space: nowrap;overflow: hidden !important; "')
            var mailheading = document.createElement('h4');
            mailheading.setAttribute("class", "panel-title");

            /**Checkbox code **/
            var accordion_input = document.createElement("input");
            accordion_input.setAttribute("type", "checkbox");
            accordion_input.setAttribute("id", "" + current_mails[i].id);
            accordion_input.setAttribute("name", "greenCheck");
            accordion_input.setAttribute("class", "pinToggles");
            var checkclick = document.createAttribute("onclick");
            checkclick.value = "checkedmails(id)";
            accordion_input.setAttributeNode(checkclick);
            //checkbox code over



            var mail_link = document.createElement('a');
            mail_link.setAttribute("role", "button");
            mail_link.setAttribute("data-toggle", "collapse");
            mail_link.setAttribute("data-parent", "#mails_accordion");
            mail_link.setAttribute("href", "#mail_" + current_mails[i].id);
            mail_link.setAttribute("aria-expanded", "true");
            mail_link.setAttribute("id", "#mail_" + current_mails[i].id);
            mail_link.setAttribute("aria-controls", "collapseOne");
            mail_link.setAttribute("style", " font-size:14px; color:#212121");
            var subject_div = document.createElement('div');
            subject_div.setAttribute("style","display:inline-block; width:450px; white-space: nowrap; overflow:hidden !important; text-overflow: ellipsis;");
            if (current_mails[i].Subject == "") {
                subject_div.innerHTML = "(no subject)";
            } else {
                subject_div.innerHTML = current_mails[i].Subject;
            }
            var from_div = document.createElement('div');
            from_div.setAttribute("class", "pull-right");
            from_div.style.color = "#000";
            from_div.innerHTML = current_mails[i].From;
            mails_accordion.appendChild(accordion_defpan); //appending main block under predefined div in html
            mail_link.appendChild(subject_div);
            mail_link.appendChild(from_div);
            //accordion_linmails[i].Subject == ""k{erHTML = mails[i].Subject;}
            var mail_body = document.createElement('div');
            mail_body.setAttribute("id", "mail_" + current_mails[i].id);
            mail_body.setAttribute("class", "panel-collapse collapse ");
            mail_body.setAttribute("aria-labelledby", "headingOne");
            var inner_div = document.createElement('div');
            inner_div.setAttribute("class", "panel-body container-fluid");
            inner_div.setAttribute("style","margin-left:20px;");
            var mailtext = document.createElement('p');
            mailtext.setAttribute("class", "span8");
            inner_div.appendChild(mailtext);
            mailtext.innerHTML = current_mails[i].body.html;
            mail_body.appendChild(inner_div);
            headpan.appendChild(mailheading);
            mailheading.appendChild(accordion_input);
            mailheading.appendChild(mail_link);
            accordion_defpan.appendChild(headpan);
            accordion_defpan.appendChild(mail_body);
        }
    } else {
        var norecordfound = document.getElementById("norecordfound");
        mails_accordion.innerHTML = "";
        //mails_accordion.innerHTML="<br/><span style=\"color:#636363\">No results found</span>"
        norecordfound.style.display = "";
    }
}







function display_error_message() {
	//if any error occured
    var loading_spinner = document.getElementById("loading_spinner");
    var loading_error = document.getElementById("loading_error");
    loading_spinner.style.display = "none";
    loading_error.style.display = "";
}


function bulk_download(label_id) {
	//handling bulk download request
    $.ajax({
        type: "GET",
        url: "/download/download_mails/?label=" + label_id,
        success: function(data){
		var modData = document.getElementById("reqresponse");		
		modData.innerHTML = data;	
	}

    });
}
//cancelling pending ajax requests

$.ajaxQ = (function(){
  var id = 0, Q = {};

  $(document).ajaxSend(function(e, jqx){
    jqx._id = ++id;
    Q[jqx._id] = jqx;
  });
  $(document).ajaxComplete(function(e, jqx){
    delete Q[jqx._id];
  });

  return {
    abortAll: function(){
      var r = [];
      $.each(Q, function(i, jqx){
        r.push(jqx._id);
        jqx.abort();
      });
      return r;
    }
  };

})();

function switch_label(label_id, label_name) {
    $.ajaxQ.abortAll();
	//handling switch label request
    document.getElementById("loading_error").style.display = "none";
    document.getElementById("norecordfound").style.display = "none";
    var mails_accordion = document.getElementById("mails_accordion");
    mails_accordion.innerHTML = "";
    spinner = document.getElementById("loading_spinner");
    spinner.style.display = "block";
    var lname = document.getElementById("labelname");
    lname.innerHTML = "";
    lname.innerHTML = "Explore your digital data&nbsp;:&nbsp;" + label_name.capitalizeFirstLetter();
    document.getElementById("bulk_download").disabled = true
    document.getElementById("filter_mails").disabled = true
      //getting the count of mails in a selected label
    $.ajax({
        type: "GET",
        url: "/api/get_count/?format=json&label=" + label_id,
        success: function(data) {
            lname.innerHTML = "Explore your digital data&nbsp;:&nbsp;" + label_name.capitalizeFirstLetter()+"("+data+")";
            var bulkmodvar = document.getElementById("bulk_download");
            var bulk_button = document.getElementById("bulk_download");
            if (data>50){
			document.getElementById("bulk_download").disabled = true
                        document.getElementById("filter_mails").disabled = true
			bulk_button.setAttribute("onclick", "bulk_download(\"" + label_id + "\")");
                        bulkmodvar.setAttribute("data-target","#bulkpop");
		}
            else{
			document.getElementById("bulk_download").disabled = true
                        document.getElementById("filter_mails").disabled = true
 			bulkmodvar.setAttribute("data-target","#myModal");
                        bulkmodvar.removeAttribute("onclick");
                }
              


        },
        error: function(data, errorThrown) {
            display_error_message();
        }
    });

    //calling the api to fetch mails
    $.ajax({
        type: "GET",
        url: "/api/list_mails/?format=json&label=" + label_id,
        success: function(data) {
            selected_mails = data;
            current_mails = selected_mails;
            previous_current_mails(current_mails)
	    	document.getElementById("bulk_download").disabled = false
            document.getElementById("filter_mails").disabled = false
            spinner.style.display = "none";
            
              


        },
        error: function(data, errorThrown) {
            display_error_message();
        }
    });



}


//bulk download function
function bulk_download_json(label_id) {
           var self = this;
            $.ajax({
                url: "mydownload.html",
                context: document.body,
                success:function(){
                setTimeout(function(){
                    $("#jquery-msg-overlay").fadeOut(self.fadeOut,$("#jquery-msg-overlay").remove)
                    },1000);
                document.getElementById(label_id).remove();
                remove_bulk_data(label_id);
               }
            });
	}


function remove_bulk_data(label_id) {
	//removing unnecessary data in database
    $.ajax({
        type: "GET",
        url: "/remove_data/?label=" + label_id,

    });
}




function download_as_json() {
    // download few mails
    var json_filename = document.getElementById("json_filename").value;
    document.getElementById("json_content").innerHTML = "";

    if (json_filename == "") {
        download_filename = "filename.json";
    } else if (json_filename.slice(-5) == ".json") {
        download_filename = json_filename;
    } else {
        download_filename = json_filename.concat(".json");
    }
    var json_file = "text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(current_mails));
    $('<a id="json_link" href="data:' + json_file + '" download="' + download_filename + '">download JSON</a>').appendTo('#json_content');
    document.getElementById('json_link').click();
}




function filter_mails(data) {
    // filtering the mails on user input
    filter_input = data;//document.getElementById("filter_input").value;
    current_mails = [];
    var words = filter_input.split(" ");
    var words_length = words.length
    var selected_length = selected_mails.length
    for (var i = 0; i < selected_length; i++) {
        for (var j = 0; j < words_length; j++) {
            if ((JSON.stringify(selected_mails[i]).toLowerCase()).indexOf(words[j].toLowerCase()) != -1) {
                current_mails.push(selected_mails[i]);
                break;
            }
        }
    }
    previous_current_mails(current_mails);
}



function checkedmails(pass) {

    //user selected mails
    current_mails = [];
    if (document.getElementById(pass).checked) {
        //appending checked mails into current mails
        for (i = 0; i < selected_mails.length; i++) {
            if ((selected_mails[i].id === pass)) {
                current_mails.push(selected_mails[i]);
                break;
            }
        }
    } 
    else {

        for (i = 0; i < current_mails.length; i++) {
            //removing unchecked mails from current_mails
            if (pass === current_mails[i].id) {
                current_mails.pop(current_mails[i]);

            }
        }

    }
}

function test(label, format) {
	var cbox_name = label+"option";
	var text_id = label+"filename"
	var name_file = document.getElementById(text_id).value;
    var cboxes = document.getElementsByName(cbox_name);
    var len = cboxes.length;
    var selected_option = [];
    for (var i=0; i<len; i++) {
    	if (cboxes[i].checked){
    		selected_option.push(cboxes[i].value);
    	}

        //alert(i + (cboxes[i].checked?' checked ':' unchecked ') + cboxes[i].value);
    }
    if (selected_option === undefined || selected_option.length == 0) {
    	
        window.location.href ="/initiate_download/?label="+label+"&format="+format+"&selected_options=all&file_name="+name_file;

    }
    else {
    	window.location.href ="/initiate_download/?label="+label+"&format="+format+"&selected_options="+selected_option+"&file_name="+name_file;
    }
}