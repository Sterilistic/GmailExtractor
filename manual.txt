########The first piece of software you'll install is Redis(The Broker). (it is included in requirements.txt)######

$ sudo aptitude install redis-server
$ redis-server --version
Redis server version 2.4.14 (00000000:0)



####### Fire up redis server ################

redis-server



########## Check if Redis is up and accepting connections: #############

$ redis-cli ping

>PONG


########## Install supervisor (It is not in requirements becuz it is supposed to be installed saperately) ########### 

$ sudo apt-get install supervisor


######## For Digital Ocean ###############

Change the entries in supervisor configuration files in folder named supervisor.

then Copy those two files into the remote server in the “/etc/supervisor/conf.d/” directory.

Create the log files that are mentioned in the configuration scripts in supervisor folder on the remote server:---if directory doesnot exist, make one------: and run these in terminal window-

$ touch <project-directory>/log/celery/newextract_worker.log
$ touch <project-directory>/log/celery/newextract_beat.log


Finally, run the following commands to make Supervisor aware of the programs-
$ sudo supervisorctl reread
$ sudo supervisorctl update       <!--with this command supervisor started and with status command check if its running-->

Run the following commands to stop, start, and/or check the status of the newextractcelery program:

$ sudo supervisorctl stop newextractcelery
$ sudo supervisorctl start newextractcelery
$ sudo supervisorctl status newextractcelery



Last-

python manage.py runserver


#####--------- NOT FOR PRODUCTION --------######
##########Three Seperate terminals command for testing-------- TO RUN LOCALLY!!!!!!!################
celery -A newextract worker -l info
celery -A newextract beat -l info
python manage.py runserver



# All the three commands in saperate terminal in the project directory.




#################################################################################
To have GUI for celery!


You can use pip to install Flower:

$ pip install flower
Running the flower command will start a web-server that you can visit:

$ celery -A proj flower
The default port is http://localhost:5555, but you can change this using the –port argument:

$ celery -A proj flower --port=5555
Broker URL can also be passed through the –broker argument :

$ celery flower --broker=amqp://guest:guest@localhost:5672//
or
$ celery flower --broker=redis://guest:guest@localhost:6379/0
Then, you can visit flower in your web browser :

$ open http://localhost:5555
Flower has many more features than are detailed here, including authorization options. Check out the official documentation for more information.

celery events: Curses Monitor
New in version 2.0.

celery events is a simple curses monitor displaying task and worker history. You can inspect the result and traceback of tasks, and it also supports some management commands like rate limiting and shutting down workers. This monitor was started as a proof of concept, and you probably want to use Flower instead.

Starting:

$ celery -A proj events
