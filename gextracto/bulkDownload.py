import httplib2
import base64
import json
import mimetypes
from time import gmtime, strftime
from stripogram import html2text
from oauth2client.django_orm import Storage
from apiclient.discovery import build
from oauth2client import client
from django.contrib.auth.models import User
from .models import CredentialsModel
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import redirect
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication
from rest_framework import permissions
from gextracto import models
from gextracto.models import UserData
from gextracto.models import BulkRequestId
from django.template import RequestContext
from rest_framework.response import Response
from django.contrib.auth.decorators import login_required
from django.core import serializers
from rest_framework.views import APIView 
from rest_framework.settings import api_settings 
from rest_framework_csv.renderers import CSVRenderer
from rest_framework_csv import *
from rest_framework.decorators import api_view
from rest_framework.decorators import renderer_classes
from django.core.servers.basehttp import FileWrapper
from djqscsv import render_to_csv_response
    


class ListDownloadRequest(APIView):
    """
    Gets a list of a specified number mail ids for a particular label
    Extracts the email in the form of plain/text
    The API returns all the extracted mails
    """
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        if not request.user.is_authenticated():
            return render(request, 'homepage.html')
        else:
            user = request.user
            storage = Storage(CredentialsModel, 'id', user, 'credential')
            credentials = storage.get()
            http_auth = credentials.authorize(httplib2.Http())
            service = build('gmail', 'v1', http=http_auth)
            user_Id = user.username
            download_request = BulkRequestId.objects.filter(request_id = user_Id)
            data = [ob.as_json() for ob in download_request]
            return Response(data)


@api_view(['GET'])
@login_required
def get(request, format=None):
    """
    handles download request and dispose the data in required file format
    """
    user = request.user
    storage = Storage(CredentialsModel, 'id', user, 'credential')
    credentials = storage.get()
    http_auth = credentials.authorize(httplib2.Http())
    service = build('gmail', 'v1', http=http_auth)
    user_Id = user.username
    label_id = request.GET['label']
    if request.GET['selected_options'] =="all":
        all_mails = UserData.objects.filter(user_id=user_Id, label=label_id)
        content = [{'user_id':data.user_id,
                    'label' : data.label,
                    'sender' : data.sender,
                    'subject' : data.subject,
                    'body' : data.body} for data in all_mails]
        response = Response(content)
        response['Content-Disposition'] = "attachment; filename=%s"%request.GET['file_name']
        return response
    fields = request.GET['selected_options']
    b = fields.split(',')
    all_mails = UserData.objects.filter(user_id=user_Id, label=label_id).values(*b)
    if request.GET['format']=="csv":
        response =  render_to_csv_response(all_mails)
        response['Content-Disposition'] = "attachment; filename=%s"%request.GET['file_name']
        return response
    response = Response(all_mails)
    response['Content-Disposition'] = "attachment; filename=%s"%request.GET['file_name']
    return response
        



class RemoveData(APIView):
    """
    Removes the redundant information from database.
    """
    authentication_classes = (authentication.SessionAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, format=None):
        """
        removing the data on user remove request
        """
    	user = request.user
        storage = Storage(CredentialsModel, 'id', user, 'credential')
        credentials = storage.get()
        http_auth = credentials.authorize(httplib2.Http())
        service = build('gmail', 'v1', http=http_auth)
        user_Id = user.username
        label_id = request.GET['label']
    	BulkRequestId.objects.filter(request_id = user_Id, label = label_id).delete()
    	UserData.objects.filter(user_id=user, label = label_id).delete()
