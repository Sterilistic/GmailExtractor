from django.db import models
from django.contrib.auth.models import User
from oauth2client.django_orm import FlowField
from oauth2client.django_orm import CredentialsField
from datetime import date
import datetime
from django.utils import timezone


class FlowModel(models.Model):
	flow = FlowField()


	
class CredentialsModel(models.Model):
	id = models.ForeignKey(User, primary_key=True, on_delete=models.CASCADE)
	credential = CredentialsField()

class UserData(models.Model):
	user_id=models.CharField(max_length=50,default='<no user_id>')
	label=models.CharField(max_length=50,default='<no label>')
	sender = models.CharField(max_length=50,default="<sender>")
	subject = models.CharField(max_length=500,default="<subject>")
	body = models.TextField(default="<body>")

	def __unicode__(self):
		return u'%s %s' % (self.user_id, self.label)
	
class BulkRequestId(models.Model):
	request_id=models.CharField(max_length=50,default="<no request_id>")	
	label=models.CharField(max_length=50,default="<no label>")
	date = models.DateField( default=timezone.now)
	flag = models.BooleanField(default=True)

	def as_json(self):
		return dict(
			request_id=self.request_id,
			label=self.label,
			date=self.date,
			flag = self.flag)

	def __unicode__(self):
		return u'%s %s' % (self.request_id, self.label)

class TaskId(models.Model):
	request_id=models.CharField(max_length=50,default="<no id>")	
	label=models.CharField(max_length=50,default="<no label>")
	date = models.DateField( default=timezone.now)

	def __unicode__(self):
		return u'%s %s' % (self.request_id, self.label)
